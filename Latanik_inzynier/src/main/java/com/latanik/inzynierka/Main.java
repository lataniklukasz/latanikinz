package com.latanik.inzynierka;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.awt.AlphaComposite;
import static java.awt.Color.blue;
import static java.awt.Color.green;
import static java.awt.Color.red;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import static java.lang.Math.atan;
import static java.lang.Math.sqrt;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;
import static javax.swing.Spring.height;
import static javax.swing.Spring.width;
import org.bytedeco.javacpp.FlyCapture2.Image;
import static org.bytedeco.javacpp.FlyCapture2.NULL;
import org.bytedeco.javacpp.opencv_core;
import static org.bytedeco.javacpp.opencv_core.CV_32FC1;
import org.bytedeco.javacpp.opencv_core.CvMat;
import org.bytedeco.javacpp.opencv_core.CvRect;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_core.Mat;
import static org.bytedeco.javacpp.opencv_core.cvCopy;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvGetMat;
import static org.bytedeco.javacpp.opencv_core.cvGetSize;
import static org.bytedeco.javacpp.opencv_core.cvResetImageROI;
import static org.bytedeco.javacpp.opencv_core.cvSave;
import static org.bytedeco.javacpp.opencv_core.cvScalar;
import static org.bytedeco.javacpp.opencv_core.cvSet;
import static org.bytedeco.javacpp.opencv_core.cvSetImageROI;
import static org.bytedeco.javacpp.opencv_core.cvSize;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvSaveImage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RGB2RGBA;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

public class Main {

    public static void printPixelARGB(int pixel) {
        int alpha = (pixel >> 24) & 0xff;
        int red = (pixel >> 16) & 0xff;
        int green = (pixel >> 8) & 0xff;
        int blue = (pixel) & 0xff;
        //System.out.println("argb: " + alpha + ", " + red + ", " + green + ", " + blue);
    }

    public static void printPixelTransparent(int pixel, int r, int g, int b, int a) {

        //System.out.println("argb: " + alpha + ", " + red + ", " + green + ", " + blue);
    }

    private static void addTransparency(BufferedImage image) {
        int w = image.getWidth();
        int h = image.getHeight();
        int r, g, b, a;

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                int pixel = image.getRGB(j, i);
                if (j < 300) {
                    a = 50; //(pixel >> 24) & 0xff;
                    r = (pixel >> 16) & 0xff;
                    g = (pixel >> 8) & 0xff;
                    b = (pixel) & 0xff;
                    int col = (a << 24) | (r << 16) | (g << 8) | b;
                    image.setRGB(j, i, col);
                }

            }
        }
    }

    public static void maluj(int r, int g, int b) {

    }

    public static void main(String[] args) throws Exception {

//      
        FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber("1.mp4");
        FFmpegFrameGrabber grabber2 = new FFmpegFrameGrabber("2.mp4");
        //  FFmpegFrameGrabber grabber3 = new FFmpegFrameGrabber("3.mp4");
        try {
            OpenCVFrameConverter.ToIplImage ipl1 = new OpenCVFrameConverter.ToIplImage();
            OpenCVFrameConverter.ToIplImage ipl2 = new OpenCVFrameConverter.ToIplImage();
            //  OpenCVFrameConverter.ToIplImage ipl3 = new OpenCVFrameConverter.ToIplImage();
            grabber1.start();
            grabber2.start();
            //  grabber3.start();
            //int frame_count = grabber.getLengthInFrames();
            for (int i = 150; i < 151; i += 1/*grabber.getFrameRate()*/) {
                if (i > 0) {
                    grabber1.setFrameNumber(i);
                    grabber2.setFrameNumber(i);
                    //         grabber3.setFrameNumber(i);
                }
                Frame frame1 = grabber1.grabImage();
                Frame frame2 = grabber2.grabImage();
                // Frame frame3 = grabber3.grabImage();
                if (frame1 == null) {
                    break;
                }
                if (frame1.image == null) {
                    continue;
                }
                IplImage image1 = ipl1.convert(frame1);
                IplImage image2 = ipl2.convert(frame2);
               // IplImage image3 = ipl3.convert(frame3);

             //   CvRect r = new CvRect(0, 300, 1500,500);
                //   CvRect r2 = new CvRect(520, 300, 1200,500);
            //    cvSetImageROI(image1, r);
                //    cvSetImageROI(image2, r2);
             //   IplImage cropped = cvCreateImage(cvSize(1500,500), IPL_DEPTH_8U, 3);
                //  IplImage cropped2 = cvCreateImage(cvSize(1200,500), IPL_DEPTH_8U, 3);
//                
//               cvCopy(image1, cropped);
//               cvCopy(image2, cropped2);
//               
//               cvSaveImage("raz.jpg",cropped);
//               cvSaveImage("dwa.jpg",cropped2);
//               
//               BufferedImage j = new BufferedImage(cropped.width(),cropped.height(),BufferedImage.TYPE_INT_ARGB);
//               
//               BufferedImage d = new BufferedImage(cropped2.width(),cropped2.height(),BufferedImage.TYPE_INT_ARGB);
//               
                //     j = ImageIO.read(new File("raz.jpg"));
                //     d = ImageIO.read(new File("dwa.jpg"));
           //    addTransparency(d);
                ryba(image1);

           //    File outputfile = new File("naprawione.png");
                //    ImageIO.write(d, "png", outputfile);
            //   IplImage merge = cvCreateImage(cvSize(3000, 500), IPL_DEPTH_8U, 3);                
            //    CvRect a = new CvRect(0, 0, 1500,500);
                //   CvRect b = new CvRect(1300, 0, 1200,500);
//                cvSetImageROI(merge, a);
//                cvCopy(cropped, merge);
//                cvResetImageROI(merge);
//                cvSetImageROI(merge, b);
//                cvCopy(cropped2, merge);
//                cvResetImageROI(merge);
              //  String img_path1 = "frame1" + String.valueOf(i) + ".jpg";
                //   String img_path2 = "frame2" + String.valueOf(i) + ".jpg";
                //   String img_path3 = "frame3" + String.valueOf(i) + ".jpg";
   //             cvSaveImage(img_path1, cropped);
                //    cvSaveImage("cos.jpg", cropped);
                //        cvSaveImage("cos2.jpg", cropped2);
                //         cvSaveImage(img_path2, image2);
//                cvSaveImage(img_path3, image3);
//               
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        grabber1.stop();
        grabber2.stop();
//      //  grabber3.stop();
//      

    }

    public static void ryba(IplImage rybie) throws IOException {

        double sila;
        sila = 2.0;
        double zoom;
        zoom = 1.0;

        int pwysokosc = rybie.width() / 2;
        int pdlugosc = rybie.height() / 2;

        int h = rybie.height();
        int w = rybie.width();

        double promien = sqrt(rybie.width() * rybie.width() + rybie.height() * rybie.height()) / sila;

        String img_path1 = "fish.jpg";
        String img_path2 = "defish.jpg";

        cvSaveImage(img_path1, rybie);
        cvSaveImage(img_path2, rybie);

        BufferedImage image1;
        BufferedImage image2;

        image1 = ImageIO.read(new File(img_path1));
        image2 = ImageIO.read(new File(img_path2));

        for (int y = 0; y < rybie.height(); y++) {
            for (int x = 0; x < rybie.width(); x++) {

                int newX = x - pdlugosc;
                int newY = y - pwysokosc;

                double distance = sqrt(newX * newX + newY * newY);
                double r = distance / promien;

                double theta;
                if (r == 0) {
                    theta = 1;
                } else {
                    theta = (atan(r) / r);
                }

                int sourceX = (int) (pdlugosc + theta * newX * zoom);
                int sourceY = (int) (pwysokosc + theta * newY * zoom);

                if( sourceX >= w )
                    sourceX = w - 1;
                
                else if(sourceX < 0)
                        sourceX = 0;
                
                 if( sourceY >= h )
                    sourceY = h - 1;
                 
                 else if(sourceY < 0)
                        sourceY = 0;
                
                int pixel = image1.getRGB(sourceX, sourceY);

                int red = (pixel >> 16) & 0xff;
                int g = (pixel >> 8) & 0xff;
                int b = (pixel) & 0xff;
                int col = (red << 16) | (g << 8) | b;
                image2.setRGB(x, y, col);

            }
        }
        File outputfile = new File("naprawione.jpg");
        ImageIO.write(image2, "jpg", outputfile);
    }

    public static void ryba2(IplImage rybie) throws IOException {
        int width = rybie.width();
        int height = rybie.height();

        String img_path1 = "fish.jpg";
        String img_path2 = "defish.jpg";

        cvSaveImage(img_path1, rybie);
        cvSaveImage(img_path2, rybie);

        BufferedImage image1;
        BufferedImage image2;

        image1 = ImageIO.read(new File(img_path1));
        image2 = ImageIO.read(new File(img_path2));

        double paramA = -0.007715; // affects only the outermost pixels of the image
        double paramB = 0.026731; // most cases only require b optimization
        double paramC = 0.0; // most uniform correction
        double paramD = 1.0 - paramA - paramB - paramC; // describes the linear scaling of the image

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int d = Math.min(width, height) / 2;    // radius of the circle

                // center of dst image
                double centerX = (width - 1) / 2.0;
                double centerY = (height - 1) / 2.0;

                // cartesian coordinates of the destination point (relative to the centre of the image)
                double deltaX = (x - centerX) / d;
                double deltaY = (y - centerY) / d;

                // distance or radius of dst image
                double dstR = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

                // distance or radius of src image (with formula)
                double srcR = (paramA * dstR * dstR * dstR + paramB * dstR * dstR + paramC * dstR + paramD) * dstR;

                // comparing old and new distance to get factor
                double factor = Math.abs(dstR / srcR);

                // coordinates in source image
                double srcXd = centerX + (deltaX * factor * d);
                double srcYd = centerY + (deltaY * factor * d);

                // no interpolation yet (just nearest point)
                int srcX = (int) srcXd;
                int srcY = (int) srcYd;

                if (srcX >= 0 && srcY >= 0 && srcX < width && srcY < height) {
                    int dstPos = y * width + x;

                    int pixel = srcY * width + srcX;

                    int red = (pixel >> 16) & 0xff;
                    int g = (pixel >> 8) & 0xff;
                    int b = (pixel) & 0xff;
                    int col = (red << 16) | (g << 8) | b;
                    image2.setRGB(x, y, col);
                }
            }
        }
        File outputfile = new File("naprawione.jpg");
        ImageIO.write(image2, "jpg", outputfile);
    }
}






   











//package com.latanik.inzynierka;
//
//import java.awt.AlphaComposite;
//import static java.awt.Color.blue;
//import static java.awt.Color.green;
//import static java.awt.Color.red;
//import java.awt.Graphics2D;
//import java.awt.geom.Ellipse2D;
//import java.awt.geom.Rectangle2D;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import static java.lang.Math.atan;
//import static java.lang.Math.sqrt;
//import java.nio.ByteBuffer;
//import javax.imageio.ImageIO;
//import static org.bytedeco.javacpp.FlyCapture2.NULL;
//import org.bytedeco.javacpp.opencv_core;
//import static org.bytedeco.javacpp.opencv_core.CV_32FC1;
//import org.bytedeco.javacpp.opencv_core.CvMat;
//import org.bytedeco.javacpp.opencv_core.CvRect;
//import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
//import org.bytedeco.javacpp.opencv_core.IplImage;
//import org.bytedeco.javacpp.opencv_core.Mat;
//import static org.bytedeco.javacpp.opencv_core.cvCopy;
//import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
//import static org.bytedeco.javacpp.opencv_core.cvGetMat;
//import static org.bytedeco.javacpp.opencv_core.cvGetSize;
//import static org.bytedeco.javacpp.opencv_core.cvResetImageROI;
//import static org.bytedeco.javacpp.opencv_core.cvScalar;
//import static org.bytedeco.javacpp.opencv_core.cvSet;
//import static org.bytedeco.javacpp.opencv_core.cvSetImageROI;
//import static org.bytedeco.javacpp.opencv_core.cvSize;
//import static org.bytedeco.javacpp.opencv_imgcodecs.cvSaveImage;
//import static org.bytedeco.javacpp.opencv_imgproc.CV_RGB2RGBA;
//import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
//import org.bytedeco.javacv.FFmpegFrameGrabber;
//import org.bytedeco.javacv.Frame;
//import org.bytedeco.javacv.OpenCVFrameConverter;
//
//public class Main {
//    
//
//    public static void main(String[] args) throws Exception {
//        FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber("1.mp4");
//        FFmpegFrameGrabber grabber2 = new FFmpegFrameGrabber("2.mp4");
//      //  FFmpegFrameGrabber grabber3 = new FFmpegFrameGrabber("3.mp4");
//        try {
//            OpenCVFrameConverter.ToIplImage ipl1 = new OpenCVFrameConverter.ToIplImage();
//            OpenCVFrameConverter.ToIplImage ipl2 = new OpenCVFrameConverter.ToIplImage();
//          //  OpenCVFrameConverter.ToIplImage ipl3 = new OpenCVFrameConverter.ToIplImage();
//            grabber1.start();
//            grabber2.start();
//          //  grabber3.start();
//            //int frame_count = grabber.getLengthInFrames();
//            for (int i = 150; i < 151; i += 1/*grabber.getFrameRate()*/) {
//                if (i > 0) {
//                    grabber1.setFrameNumber(i);
//                    grabber2.setFrameNumber(i);
//           //         grabber3.setFrameNumber(i);
//                }
//                Frame frame1 = grabber1.grabImage();
//                Frame frame2 = grabber2.grabImage();
//               // Frame frame3 = grabber3.grabImage();
//                if (frame1 == null) {
//                    break;
//                }
//                if (frame1.image == null) {
//                    continue;
//                }
//                IplImage image1 = ipl1.convert(frame1);
//                IplImage image2 = ipl2.convert(frame2);
//               // IplImage image3 = ipl3.convert(frame3);
//                
//                CvRect r = new CvRect(0, 300, 1500,500);
//                CvRect r2 = new CvRect(520, 300, 1200,500);
//                
//                cvSetImageROI(image1, r);
//                cvSetImageROI(image2, r2);
//                
//               
//                
//                IplImage cropped = cvCreateImage(cvSize(1500,500), IPL_DEPTH_8U, 3);
//                IplImage cropped2 = cvCreateImage(cvSize(1200,500), IPL_DEPTH_8U, 3);
//                
//                cvCopy(image1, cropped);
//                cvCopy(image2, cropped2);
//                
//                IplImage merge = cvCreateImage(cvSize(3000, 500), IPL_DEPTH_8U, 3);
//                
//               // cvSet(cropped, cvScalar(0,0,0,0.5));
//              //  cvCvtColor(merge, cropped, CV_RGB2RGBA);
//                
//                CvRect a = new CvRect(0, 0, 1500,500);
//                CvRect b = new CvRect(1300, 0, 1200,500);
//                
//                
//                
//              
//                
//                cvSetImageROI(merge, a);
//                cvCopy(cropped, merge);
//                cvResetImageROI(merge);
//                cvSetImageROI(merge, b);
//                cvCopy(cropped2, merge);
//                cvResetImageROI(merge);
//                
//                
//                
//    
// 
//           
//              
//                
//                
//
//                
//                String img_path1 = "frame1" + String.valueOf(i) + ".jpg";
//                String img_path2 = "frame2" + String.valueOf(i) + ".jpg";
//                String img_path3 = "frame3" + String.valueOf(i) + ".jpg";
//                
//                
//                //cvSaveImage(img_path1, cropped);
//                cvSaveImage("cos.jpg", cropped);
//               // cvSaveImage("cos2.jpg", cropped2);
//                //cvSaveImage(img_path2, image2);
//               // cvSaveImage(img_path3, image3);
//               
//               
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        grabber1.stop();
//        grabber2.stop();
//      //  grabber3.stop();
//    }
//
//public void ryba(IplImage rybie)
//{
//    IplImage dst;
//    dst = cvCreateImage(cvSize(rybie.height(),rybie.width()), rybie.depth(), rybie.nChannels());
//    
//    double sila;
//    sila =0.3;
//    double zoom;
//    zoom=1;
//    
//    int pwysokosc = rybie.width()/2;
//    int pdlugosc = rybie.height()/2;
//    
//    double promien = Math.sqrt(rybie.width() * rybie.width() + rybie.height() * rybie.height());
//   
//    ByteBuffer buffer = rybie.getByteBuffer();
//    ByteBuffer buffer2 = dst.getByteBuffer();
//   // ByteBuffer buffer2 = dst.getByteBuffer();
//
//for(int y = 0; y < rybie.height(); y++) {
//    for(int x = 0; x < rybie.width(); x++) {
//        
//        int newX = x - pdlugosc;
//        int newY = y - pwysokosc;
//        
//        double distance =  Math.sqrt(newX*newX + newY*newY);
//        double r = distance / promien;
//        
//        double theta;
//        if (r == 0)
//        {
//            theta =1;
//        }
//        else{
//             theta = (Math.atan(r)/r);
//        }
//        
//        double sourceX = pdlugosc+theta*newX*zoom;
//        double sourceY = pwysokosc+theta*newY*zoom;
//   
//        int index = (int) (sourceY * rybie.widthStep() + sourceX*rybie.nChannels());
//        int index2 = (int) (y * dst.widthStep() + x*dst.nChannels());
//        
//
//        // Used to read the pixel value - the 0xFF is needed to cast from
//        // an unsigned byte to an int.
//        int value = buffer.get(index) & 0xFF;
//
//   
//        // Sets the pixel to a value (greyscale).
//        //buffer2.put(index, (byte) value);
//
//        // Sets the pixel to a value (RGB, stored in BGR order).
//        buffer2.put(index2, (byte) blue.getBlue());
//        buffer2.put(index2 + 1, (byte) green.getGreen());
//        buffer2.put(index2 + 2, (byte) red.getRed());
//        
//    
//        
////        buffer2.put(index2,buffer.get(index));
////        buffer2.put(index2+1,buffer.get(index));
////        buffer2.put(index2+2,buffer.get(index));
//    }
//  
//}
//    
//    
//    
//}
//
//}
//
//
//
//   
